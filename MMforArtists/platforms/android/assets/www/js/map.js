
var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat:41.3850639, lng: 2.1734035},
    zoom: 12,
  });




  var image = '../img/pin1.png';

  var beachMarker = new google.maps.Marker({
    position: {lat: 41.3850631, lng: 2.1734031},
    map: map,

  });

  var beachMarker2 = new google.maps.Marker({
    position: {lat: 41.3790369, lng: 2.1401683000000276},
    map: map,

  });

  var beachMarker3 = new google.maps.Marker({
    position: {lat: 41.380721, lng:  2.167836},
    map: map,
    icon: image
  });

  var beachMarker4 = new google.maps.Marker({
    position: {lat: 41.381124, lng: 2.150863},
    map: map,
    icon: image
  });
  var beachMarker5 = new google.maps.Marker({
    position: {lat: 41.3532459, lng: 2.153463},
    map: map,
    icon: image
  });
  var beachMarker6 = new google.maps.Marker({
    position: {lat: 41.35369, lng: 2.140345000276},
    map: map,
    icon: image
  });
  var beachMarker7 = new google.maps.Marker({
    position: {lat: 41.334569, lng: 2.140350000276},
    map: map,
    icon: image
  });
  var beachMarker8 = new google.maps.Marker({
    position: {lat: 41.3790369, lng: 2.1401683000000276},
    map: map,

  });



  };




  function geoFindMe() {
    var output = document.getElementById("out");

    if (!navigator.geolocation){
      output.innerHTML = "<p>Geolocation is not supported by your phone</p>";
      return;
    }

    function success(position) {
      var latitude  = position.coords.latitude;
      var longitude = position.coords.longitude;

      //output.innerHTML = '<p>Latitude is ' + latitude + '° <br>Longitude is ' + longitude + '°</p>';
  var image2 = '../img/pin3.png';
      var beachMarker8 = new google.maps.Marker({
        position: {lat: latitude, lng: longitude},
        map: map,

      });

    };

    function error() {
      output.innerHTML = "Unable to retrieve your location";
    };

    output.innerHTML = "<p>Locating…</p>";

    navigator.geolocation.getCurrentPosition(success, error);  };
