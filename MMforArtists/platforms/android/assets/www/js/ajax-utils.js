function _json_request_root(type, url, data, callback) {
	if(type=="GET")
		callback = data;
	req = {
		"url": url,
		"type": type,
		"complete": function(data) {
			obj = JSON.parse(data.responseText);
			callback(obj);
		},
		"crossDomain": false,
		"beforeSend": function(xhr) {
			xhr.setRequestHeader('X-CSRFToken', $.cookie("csrftoken"));
		},
		"dataType": "json",
		"contentType": "application/json",
	};
	if(type=="POST")
		req["data"] = JSON.stringify(data);
	$.ajax(req);
}

function json_request(type, url, data, callback) {
    _json_request_root(type, "https://www.monkingme.com/api/"+url+"/", data, callback);
}

function validateEmail(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

